﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ElfYourself
{
	public partial class HomePage : ContentPage
	{
		public HomePage()
		{
			InitializeComponent();
		}

		async void EnterButton_Clicked(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new ElfYourselfPage());
		}
	}
}
