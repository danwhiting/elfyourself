﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElfYourself {
    class NameDatabase {
        public static Dictionary<string, string> firstNames = new Dictionary<string, string>() {
            {"a", "Charmer" },
            {"b", "Chugginbells" },
            {"c", "Gingerkin" },
            {"d", "Cinnawhirl"},
            {"e", "Dimples" },
            {"f", "Flakenbake" },
            {"g", "Glittersby" },
            {"h", "Jolly-Bolly" },
            {"i", "Icicles" },
            {"j", "Everstickin" },
            {"k", "Pixypants" },
            {"l", "Tiny" },
            {"m", "Snowbuddy" },
            {"n", "Cocoa" },
            {"o", "Blitzer"},
            {"p", "Powders" },
            {"q", "Gumdrop" },
            {"r", "Ringading" },
            {"s", "Sugartwinks" },
            {"t", "Twinkles" },
            {"u", "Sprinkly" },
            {"v", "Tinsely" },
            {"w", "Gingers" },
            {"x", "Mittenslee" },
            {"y", "Yuleloggin" },
            {"z", "Nogger" }
        };

        public static Dictionary<string, string> lastNames = new Dictionary<string, string>() {
            {"a", "Naughtywagon" },
            {"b", "Chestnutzintrap" },
            {"c", "McCiderpie" },
            {"d", "der Nogchompin"},
            {"e", "Chocolatsoks" },
            {"f", "Figgypuddin" },
			{"g", "O'Glittersby" },
            {"h", "der Rosycheekers" },
            {"i", "O'Hoolagrahams" },
            {"j", "van Pinespice" },
            {"k", "Wondertoes" },
            {"l", "McNogginplume" },
            {"m", "McSugarsticks" },
            {"n", "van Nippyshoppin" },
            {"o", "Goosengreets"},
            {"p", "Kringlesnow" },
            {"q", "Gumdropperkins" },
            {"r", "Rinkinsocks" },
            {"s", "McBlusteryface" },
            {"t", "von Toboggan" },
            {"u", "von Mangerly" },
            {"v", "van Garlandgum" },
            {"w", "McSweaterweathers" },
            {"x", "McTrimmins" },
            {"y", "der Yuletide" },
            {"z", "der Nickenheimsticks" }
        };
    }
}
