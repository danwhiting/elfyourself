﻿using System;
using Xamarin.Forms;

namespace ElfYourself
{
	public partial class ElfYourselfPage : ContentPage
	{
		public ElfYourselfPage()
		{
			InitializeComponent();

			FirstLetterEntry.TextChanged += Handle_TextChanged;
			LastLetterEntry.TextChanged += Handle_TextChanged;

		    ElfYourselfButton.Clicked += ElfYourselfButton_OnClicked;
		}


	    async void ElfYourselfButton_OnClicked(object sender, EventArgs e) {
			try
			{
				string firstLetter = FirstLetterEntry.Text.ToLower();
				string lastLetter = LastLetterEntry.Text.ToLower();

				if (NameDatabase.firstNames.ContainsKey(firstLetter) &&
					NameDatabase.lastNames.ContainsKey(lastLetter))
				{

					ElfName.Text = string.Format("You're Elf name is {0} {1}!", NameDatabase.firstNames[firstLetter],
												 NameDatabase.lastNames[lastLetter]);
					//Santa.ScaleTo(1.5, 1000, Easing.BounceIn);
					//Santa.ScaleTo(1, 500, Easing.BounceOut);
					await Santa.RotateTo(360, 800);
					await Santa.ScaleTo(1.5, 400);
					await Santa.ScaleTo(1, 400);

				}
				else {

					await DisplayAlert("Whoops!",
						"Only enter ONE letter per box. So, if you're name is John Smith, you enter 'J' in the first box and 'S' in the second box. Happy hunting!",
						"Got it!");
				}
			}
			catch
			{ 
				await DisplayAlert("Yikes!",
						"Gotta enter somethin in that there box, youngin'!",
				                   "Got it!");
			}
        }

		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			Entry entry = sender as Entry;
			string val = entry.Text; 
			int restrictCount = 1;

			if (val.Length > restrictCount)
			{
				val = val.Remove(val.Length - 1);
				entry.Text = val; 
			}
		}
    }
}